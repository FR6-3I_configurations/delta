# Delta

## Dépendances (Arch)

Paquets officiels :

```shell
pacman -S git-delta
```

## Configuration

Inclure la configuration delta à la configuration git :

```shell
[include]
    path = ~/.config/delta/delta.gitconfig
```
